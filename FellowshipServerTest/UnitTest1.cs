using System;
using System.IO;
using System.Net.Sockets;
using Xunit;

using FellowshipServer.HttpBackend;
using System.Net;
using FellowshipServer.FellowshipGame;
using System.Collections.Generic;

namespace FellowshipServerTest
{
    public class UnitTest1
    {
        private static readonly string Content = "GET /?name=justin&class=mage HTTP/1.1\r\n" +
            "User-Agent: Nightbot-URL-Fetcher/0.0.3\r\n" +
            "Nightbot-User: name=jhard9000&displayName=Ochiva_&provider=twitch&providerId=138998093&userLevel=owner\r\n" +
            "Nightbot-Channel: name=jhard9000&displayName=Ochiva_&provider=twitch&providerId=138998093\r\n" +
            "Nightbot-Response-Url: https://api.nightbot.tv/1/channel/send/TVRZeE1qQXpPRFl6TURrME5DOTBkMmwwWTJndk5qQXdZMk5pWVdSaVpqUmhPR0V5WWpWbE0yWTVaR1ZrTDJGM2N3OjczZjk3NTcyMTZhNzdmNjc4ZjkxYzAzZmUxMTZhZWQwOWQzYjUxYzVmZGJjZTJhMjY2ZmZkNDRjNjBlNmUwZWU\r\n" +
            "host: 184.3.197.189\r\n" +
            "Connection: close\r\n";

        private static readonly string StreamElementsContent = "GET /?user=jhard9000&class=mage HTTP/1.1\r\n" +
            "User-Agent: StreamElements Bot\r\n" +
            "x-streamelements-channel: 5f976ae174118f7540ac655c\r\n" +
            "accept-encoding: gzip, deflate" +
            "Host: 184.3.197.189:9000\r\n" +
            "Connection: close\r\n";

        [Fact]
        public void HttpStreamServerTest()
        {
            using (MemoryStream memStream = new MemoryStream(500))
            {
                StreamWriter writer = new StreamWriter(memStream);
                writer.WriteLine(Content);
                writer.Flush();

                memStream.Seek(0, SeekOrigin.Begin);
                HttpStreamServer server = new HttpStreamServer(memStream);

                Assert.Equal("GET", server.Method);
                Assert.Equal("HTTP/1.1", server.Protocol);
                Assert.True(server.UrlParameters.ContainsKey("name"));
                Assert.Equal("justin", server.UrlParameters["name"]);
                Assert.True(server.UrlParameters.ContainsKey("class"));
                Assert.Equal("mage", server.UrlParameters["class"]);

                long position = memStream.Position;

                server.SendResponce("wow");
                server.Flush();
                memStream.Seek(position, SeekOrigin.Begin);

                StreamReader reader = new StreamReader(memStream);

                Assert.Equal("HTTP/1.1 200 OK", reader.ReadLine());
                Assert.Equal("Content-Type: text/html; charset=utf-8", reader.ReadLine());
                Assert.Equal("", reader.ReadLine());
                Assert.Equal("wow", reader.ReadLine());
            }
        }

        [Fact]
        public void NightBotStreamServerTest()
        {
            using (MemoryStream memStream = new MemoryStream(500))
            {
                StreamWriter writer = new StreamWriter(memStream);
                writer.WriteLine(Content);
                writer.Flush();

                memStream.Seek(0, SeekOrigin.Begin);
                NightBotStreamServer server = new NightBotStreamServer(memStream);

                Assert.Equal("jhard9000", server.UserName);
                Assert.Equal("Ochiva_", server.UserDisplayName);
                Assert.Equal("owner", server.UserLevel);
                Assert.Equal("jhard9000", server.ChannelName);
                Assert.Equal("Ochiva_", server.ChannelDisplayName);
            }
        }

        [Fact]
        public void StreamElementsBotStreamServerrTest()
        {
            using (MemoryStream memStream = new MemoryStream(500))
            {
                StreamWriter writer = new StreamWriter(memStream);
                writer.WriteLine(StreamElementsContent);
                writer.Flush();

                memStream.Seek(0, SeekOrigin.Begin);
                var server = new StreamElementsBotStreamServer(memStream);

                Assert.Equal("jhard9000", server.UserName);
            }
        }

        [Fact]
        public void JoinFellowshipTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");

            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            string response = jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            Assert.Equal("Welcome to the fellowship NeckBeard99!", response);
            Assert.Single(jhardRPG.ActiveKnights);
            Assert.Equal(jhardRPG.ActiveKnights, jhardRPG.ActiveKnights);
            Assert.Equal("NeckBeard99", jhardRPG.ActiveKnights[0].Name);
            Assert.Equal("boy", jhardRPG.ActiveKnights[0].Gender);
        }

        [Fact]
        public void JoinFellowshipBadInput()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");

            Dictionary<string, string> args = new Dictionary<string, string>();

            //Missing parameter
            string response = jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            //Bad parameter value
            args["gender"] = "non-binary";
            response = jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);
        }

        [Fact]
        public void GetStatsTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");

            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);
            string reponse = jhardRPG.ExecuteAction(ChatRPG.GetStatsAction, "NeckBeard99", null);

            Assert.Equal("Character Stats - Name: NeckBeard99, Level: 1, Health: 10/10, Attack: 1, Defense: 1, Exp: 0, Exp to Level: 114, Hard Coin: 10, Weapon: Wooden Sword", reponse);
        }

        [Fact]
        public void AddMonsterTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            args["monsterName"] = "Slime";
            args["monsterLevel"] = "1";

            String reponse = jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);

            Assert.Equal(jhardRPG.Monsters[0].Name + " has entered the battle field, knights attack!", reponse);
            Assert.Single(jhardRPG.Monsters);
            Assert.Equal("Slime", jhardRPG.Monsters[0].Name);
            Assert.True(jhardRPG.Monsters[0].Health > 0);
            Assert.True(jhardRPG.Monsters[0].Attack > 0);
            Assert.True(jhardRPG.Monsters[0].Defense > 0);
            Assert.True(jhardRPG.Monsters[0].RewardExp > 0);
        }

        [Fact]
        public void AddMonsterBadInputTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            //Missing arguments
            string reponse = jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);

            args["monsterLevel"] = "1";
            reponse = jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);

            //Bad values
            args["monsterName"] = "234235";
            jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);

            args["monsterName"] = "Slime";
            args["monsterLevel"] = "4h";
            jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);

            Assert.Single(jhardRPG.Monsters);
            Assert.Equal(1, jhardRPG.Monsters[0].Level);
        }

        [Fact]
        public void AddLevelsToPlayerByFellowshipTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");

            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            args["levelsToAdd"] = "5";
            String reponse = jhardRPG.ExecuteAction(ChatRPG.AddLevelsToKnightAction, "NeckBeard99", args);

            Assert.Equal("A Fellowship Leader has granted " + jhardRPG.ActiveKnights[0].Name + " with 5 levels!", reponse);
            Assert.Equal(6, jhardRPG.ActiveKnights[0].Level);
            Assert.Equal(37, jhardRPG.ActiveKnights[0].Health);
            Assert.Equal(6, jhardRPG.ActiveKnights[0].Attack);
            Assert.Equal(6, jhardRPG.ActiveKnights[0].Defense);
        }

        [Fact]
        public void AddLevelsToPlayerByFellowshipBadInputTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");

            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            //Missing arguments
            string reponse = jhardRPG.ExecuteAction(ChatRPG.AddLevelsToKnightAction, "NeckBeard99", args);

            //Bad values
            args["levelsToAdd"] = "five";
            jhardRPG.ExecuteAction(ChatRPG.AddLevelsToKnightAction, "NeckBeard99", args);
        }

        [Fact]
        public void PlayerAttacksMonsterTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");

            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);


            args["monsterName"] = "Slime";
            args["monsterLevel"] = "1";
            jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);


            args["attacksMonster"] = "0";
            string response = jhardRPG.ExecuteAction(ChatRPG.AttackMonsterAction, "NeckBeard99", args);

            Assert.True(jhardRPG.ActiveKnights[0].Health < 10);
            Assert.True(jhardRPG.Monsters[0].Health < 13);
        }

        [Fact]
        public void PlayerLevelUpByMonsterTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";
            jhardRPG.JoinFellowship("NeckBeard99", args);

            args["monsterName"] = "Slime";
            args["monsterLevel"] = "1";
            jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);
            jhardRPG.ExecuteAction(ChatRPG.AddMonsterAction, "NeckBeard99", args);

            args["attacksMonster"] = "0";

            while (jhardRPG.Monsters.Count > 0) {
                jhardRPG.ExecuteAction(ChatRPG.AttackMonsterAction, "NeckBeard99", args);
                jhardRPG.ActiveKnights[0].Health += 1;
            }

            Assert.Equal(2, jhardRPG.ActiveKnights[0].Level);
            Assert.Equal(0, jhardRPG.ActiveKnights[0].CurrentExp);
            Assert.Empty(jhardRPG.Monsters);
        }

        [Fact]
        public void AddWeaponTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";

            jhardRPG.JoinFellowship("NeckBeard99", args);

            args["weaponName"] = "JagSword";
            string response = jhardRPG.ExecuteAction(ChatRPG.AddWeaponAction, "NeckBeard99", args);

            Assert.Equal("A JagSword weapon has been discovered!", response);
            Assert.Single(jhardRPG.Items);
        }

        [Fact]
        public void AddSpellTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();
            args["gender"] = "boy";

            jhardRPG.JoinFellowship("NeckBeard99", args);

            args["spellName"] = "Fire Attack";
            string response = jhardRPG.ExecuteAction(ChatRPG.AddSpellAction, "NeckBeard99", args);

            Assert.Equal("A Fire Attack spell has been discovered!", response);
            Assert.Single(jhardRPG.Items);
        }

        [Fact]
        public void BuyItemTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();

            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            args["spellName"] = "Fire Attack";
            jhardRPG.AddSpell(args);

            args["itemSlot"] = "0";
            string response = jhardRPG.ExecuteAction(ChatRPG.BuyItemAction, jhardRPG.ActiveKnights[0].Name, args);
            Assert.Equal(jhardRPG.ActiveKnights[0].Name + " bought Fire Attack at a great price! " + jhardRPG.ActiveKnights[0].Name + " has 9 Hard Coins left!", response);
            Assert.Empty(jhardRPG.Items);
        }

        [Fact]
        public void CastSpellTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();

            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);

            args["spellName"] = "Fire Attack";
            jhardRPG.AddSpell(args);

            args["itemSlot"] = "0";
            jhardRPG.BuyItem(jhardRPG.ActiveKnights[0].Name, args);
            args["monsterName"] = "Dummy";
            args["monsterLevel"] = "10";
            jhardRPG.AddMonster(args);

            args["target"] = "0";

            string response = jhardRPG.ExecuteAction(ChatRPG.CastAction, jhardRPG.ActiveKnights[0].Name, args);
            Assert.True(jhardRPG.Monsters[0].Health < jhardRPG.Monsters[0].MaxHealth);

        }

        [Fact]
        public void CastSpellOnAllyTest()
        {
            ChatRPG jhardRPG = new ChatRPG("Jhard9000");
            Dictionary<string, string> args = new Dictionary<string, string>();

            args["gender"] = "boy";
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "NeckBeard99", args);
            jhardRPG.ExecuteAction(ChatRPG.JoinFellowshipAction, "Cyb3rQt", args);
            jhardRPG.ActiveKnights[1].Health -= 5;

            args["spellName"] = "Healing Light";
            jhardRPG.AddSpell(args);

            args["itemSlot"] = "0";
            jhardRPG.BuyItem(jhardRPG.ActiveKnights[0].Name, args);

            args["target"] = "Cyb3rQt";
            string response = jhardRPG.ExecuteAction(ChatRPG.CastAction, jhardRPG.ActiveKnights[0].Name, args);
        }
    }
}
