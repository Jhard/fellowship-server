﻿using FellowshipServer.FellowshipGame;
using FellowshipServer.HttpBackend;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FellowshipServer
{
    class Program
    {
        private static readonly int listenPort = 9000;
        private static readonly int overlayPort = 9002;
        private static List<OverlayUpdater> overlays;
        private static ChatRPG jhardRPG;

        static void Main(string[] args)
        {
            overlays = new List<OverlayUpdater>();

            var thread = new Thread(OverlayService);
            thread.Start();
            try
            {
                var listener = new TcpListener(IPAddress.Any, listenPort);
                listener.Start();

                jhardRPG = new ChatRPG("Jhard9000");
                jhardRPG.Start();

                while (true)
                {
                    Console.WriteLine("Waiting for connection...");

                    TcpClient client = listener.AcceptTcpClient();

                    StreamElementsBotStreamServer server = null;
                    try
                    {
                        server = new StreamElementsBotStreamServer(client.GetStream());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        Console.WriteLine("Awkward disconnect");
                        continue;
                    }

                    string responce = "Not a command";
                    if (server.UrlParameters.ContainsKey("cmd"))
                    {
                        System.Console.WriteLine(server.UrlParameters["cmd"]);
                        responce = jhardRPG.ExecuteAction(server.UrlParameters["cmd"], server.UserName, server.UrlParameters);
                        jhardRPG.PostCommandUpdate();
                    }

                    server.SendResponce(responce);

                    server.Close();
                    client.Close();
                }
                /*listener.Stop();
                client.Close();
                Console.WriteLine(message);
                Console.ReadKey();*/
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void OverlayService()
        {
            var listener = new TcpListener(IPAddress.Any, overlayPort);
            listener.Start();

            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();

                var overlay = new OverlayUpdater(client);
                overlays.Add(overlay);
                jhardRPG.VisualUpdate += overlay.OnOverlayUpdate;

                Console.WriteLine("Overlay connected!");
            }
        }
    }
}
