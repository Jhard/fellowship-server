﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Channels;

namespace FellowshipServer.FellowshipGame
{
    public class ChatRPG
    {
        public event EventHandler<OverlayCommandEventArgs> VisualUpdate;

        public static readonly string JoinFellowshipAction = "!joinfellowship";
        public static readonly string GetStatsAction = "!stats";
        public static readonly string AddMonsterAction = "!addmonster";
        public static readonly string AddLevelsToKnightAction = "!levelknight";
        public static readonly string AttackMonsterAction = "!strike";
        public static readonly string AddWeaponAction = "!addweapon";
        public static readonly string AddSpellAction = "!addspell";
        public static readonly string BuyItemAction = "!buyitem";
        public static readonly string CastAction = "!cast";

        private RPGSerializer gameData;

        private List<Knight> activeKnights;
        private List<Monster> monsters;
        private List<Item> items;

        public ReadOnlyCollection<Knight> ActiveKnights;
        public ReadOnlyCollection<Monster> Monsters;
        public ReadOnlyCollection<Item> Items;

        public string ChannelName { get; private set; }

        public ChatRPG(string channelName)
        {
            ChannelName = channelName;
            activeKnights = new List<Knight>();
            ActiveKnights = activeKnights.AsReadOnly();
            monsters = new List<Monster>();
            Monsters = monsters.AsReadOnly();
            items = new List<Item>();
            Items = items.AsReadOnly();
            gameData = new RPGSerializer();
            gameData.LoadAllResources();
        }

        public void Start()
        {
            gameData = new RPGSerializer();
            gameData.LoadAllResources();
        }

        public string ExecuteAction(string command, string userName, IDictionary<string, string> args)
        {
            string responce = PerCommandCheck(command, userName, args);

            if (responce != null)
            {
                return responce;
            }

            if (string.Equals(command, JoinFellowshipAction, StringComparison.OrdinalIgnoreCase))
            {
                return JoinFellowship(userName, args);
            }
            else if (string.Equals(command, GetStatsAction, StringComparison.OrdinalIgnoreCase))
            {
                return GetKnightStats(userName);
            }
            else if (string.Equals(command, AddMonsterAction, StringComparison.OrdinalIgnoreCase))
            {
                return AddMonster(args);
            }
            else if (string.Equals(command, AddLevelsToKnightAction, StringComparison.OrdinalIgnoreCase))
            {
                return AddLevelsToKnight(userName, args);
            }
            else if (string.Equals(command, AttackMonsterAction, StringComparison.OrdinalIgnoreCase))
            {
                return AttackMonster(userName, args);
            }
            else if (string.Equals(command, AddWeaponAction, StringComparison.OrdinalIgnoreCase))
            {
                return AddWeapon(args);
            }
            else if (string.Equals(command, AddSpellAction, StringComparison.OrdinalIgnoreCase))
            {
                return AddSpell(args);
            }
            else if (string.Equals(command, BuyItemAction, StringComparison.OrdinalIgnoreCase))
            {
                return BuyItem(userName, args);
            }
            else if (string.Equals(command, CastAction, StringComparison.OrdinalIgnoreCase))
            {
                return Cast(userName, args);
            }
            return "";
        }

        public string JoinFellowship(string userName, IDictionary<string, string> args)
        {
            if(!args.ContainsKey("gender"))
            {
                return "Please choose character. You can pick boy, girl, or cat!";
            }

            string gender = args["gender"].ToLower();

            if (gender != "boy" && gender != "girl" && gender != "cat")
            {
                return "Sorry we don't understand that gender yet, but we are learning! please choose boy, girl, or cat";
            }

            Knight knight = gameData.FindKnight(userName);

            if (knight != null)
            {
                return "No need to join " + userName + " you are already a hard knight!";
            }

            knight = new Knight(userName, gender);

            gameData.AddKnightData(knight);
            activeKnights.Add(knight);
            return "Welcome to the fellowship <NAME>!".Replace("<NAME>",knight.Name);
        }

        public string GetKnightStats(string userName) {
            Knight knight = activeKnights.Find(k => string.Equals(k.Name, userName));
            return knight.getStats() + ", Weapon: " + knight.Weapon.Name + ", Spell: " + knight.MagicSpell?.Name;
        }

        public string AddMonster(IDictionary<string, string> args) {
            if (!args.ContainsKey("monsterName"))
            {
                return "Our monsters have names too you know";
            }

            string monsterName = args["monsterName"];
            Monster monster = gameData.FindMonster(monsterName);
            if (monster == null)
            {
                return "Sorry, we don't have that monster yet";
            }

            if (args.ContainsKey("monsterLevel"))
            {
                uint monsterLevel;
                if (!uint.TryParse(args["monsterLevel"], out monsterLevel))
                {
                    monsterLevel = 1;
                }
                monster.SetObjectLevel((int)monsterLevel);
            }

            monsters.Add(monster);

            TriggerEvent("addmonster " + monster.Name);

            return monster.Name + " has entered the battle field, knights attack!";
        }

        public string AddLevelsToKnight(string username, IDictionary<string, string> args) {
            if (!args.ContainsKey("levelsToAdd"))
            {
                return "Levels are numbers, you give a NUMBER!";
            }

            if (!uint.TryParse(args["levelsToAdd"], out uint levels))
            {
                return "It needs to be a POSITIVE NUMBER!";
            }

            FindActiveKnight(username).AddLevelByFellowship((int)levels);
            return "A Fellowship Leader has granted " + username + " with " + levels.ToString() + " levels!";
        }

        public string AttackMonster(string username, IDictionary<string, string> args) 
        {
            uint target = 0;
            if (args.ContainsKey("target"))
            {
                uint.TryParse(args["target"], out target);
            }

            Knight knight = FindActiveKnight(username);

            if (knight.Health <= 0)
            {
                return "Sorry, but you cannot attack because you're DEAD!";
            }

            return AttackMonster(knight, (int)target, knight.Weapon == null ? knight.Weapon.AttackDamage : 1);
        }

        private string AttackMonster(Knight knight, int monsterIndex, int baseDamage)
        {

            if (knight == null)
            {
                return "Serve error, the server lost the knight.";
            }
            else if (monsterIndex >= Monsters.Count)
            {
                return "There is no monster there to target.";
            }

            Monster monster = monsters[monsterIndex];
            string responce = "";

            int damage = CalculateDamage(knight.Attack, monster.Defense, baseDamage);
            monster.Health -= damage;

            if (monster.Health > 0)
            {
                responce += knight.Name + " dealt " + damage + " damage to " + monster.Name + "! " +
                    monster.Name + " has " + monster.Health + " HP left.";

                damage = CalculateDamage(monster.Attack, knight.Defense, monster.Attack);
                knight.Health -= damage;

                responce += " " + monster.Name + " dealt " + damage + " damage to " + knight.Name + "! " + 
                    knight.Name + " has " + knight.Health + " HP left.";

                TriggerEvent("monsterhit " + monsterIndex);
            }
            else
            {
                responce += knight.Name + " has slain " + monster.Name + "!";

                int oldLevel = knight.Level;
                knight.AddExperience(monster.RewardExp, monster);

                if (knight.Level > oldLevel)
                {
                    responce += " " + knight.Name + " leveled up to level " + knight.Level + ".";
                }

                monsters.RemoveAt(monsterIndex);
                TriggerEvent("monsterkilled " + monsterIndex);
            }

            return responce;
        }

        private static int CalculateDamage(int attack, int defence, int baseDamage)
        {
            if (attack + defence <= 0)
            {
                return 0;
            }

            return Math.Max((int)((baseDamage + attack * .8f) * ((float)attack / (attack + defence))), 1);
        }

        public string Cast(string userName, IDictionary<string, string> args)
        {
            string target = "0";
            if (args.ContainsKey("target"))
            {
                target = args["target"];
            }

            Knight knight = FindActiveKnight(userName);

            if (knight.MagicSpell == null)
            {
                return "Sorry, but you don't have a spell equipped";
            }

            string responce = "";

            if (knight.MagicSpell.Damage > 0)
            {
                if (int.TryParse(target, out int monsterIndex))
                {
                    responce += AttackMonster(knight, monsterIndex, knight.MagicSpell.Damage);

                    //target yourself if you're also doing damage
                    responce += " " + CastNoDamage(knight, knight.MagicSpell);
                }
                else
                {
                    responce += "There is no monster there, where are you aiming?";
                }

                return responce;
            }
            else
            {
                //If the spell is doing no damage then you can target another player
                Knight targetKnight = FindActiveKnight(target);
                if (targetKnight != null)
                {
                    responce += CastNoDamage(targetKnight, knight.MagicSpell);
                }
                else
                {
                    responce += CastNoDamage(knight, knight.MagicSpell);
                }

            }

            //string responce = knight.Cast(this, target);

            return responce;
        }

        private string CastNoDamage(Knight target, Spell spell)
        {
            if (spell.Heal > 0)
            {
                int oldHealth = target.Health;
                target.Health += spell.Heal;
                return "\n" + target.Name + " was healed " + (target.Health - oldHealth) + " Hp.";
            }

            return "";
        }

        public string AddWeapon(IDictionary<string, string> args)
        {
            if (!args.ContainsKey("weaponName"))
            {
                return "How is anyone supposed to know what kind of weapon you want add if you don't give a name?";
            }

            Weapon weapon = gameData.FindWeapon(args["weaponName"]);

            if (weapon == null)
            {
                return "This weapon you speak of. It does not exist...";
            }

            items.Add(weapon);
            TriggerEvent("additem " + weapon.Name);
            return "A " + weapon.Name + " weapon has been discovered!";
        }

        public string AddSpell(IDictionary<string, string> args)
        {
            if (!args.ContainsKey("spellName"))
            {
                return "What spell do you want? You should give a name";
            }

            Spell spell = gameData.FindSpell(args["spellName"]);

            if (spell == null)
            {
                return "This spell you speak of. It does not exist...";
            }

            items.Add(spell);
            TriggerEvent("additem " + spell.Name);
            return "A " + spell.Name + " spell has been discovered!";
        }

        public string BuyItem(string userName, IDictionary<string, string> args) {

            uint itemSlot = 0;
            if (args.ContainsKey("itemSlot"))
            {
                uint.TryParse(args["itemSlot"], out itemSlot);
            }

            if (items.Count <= itemSlot)
            {
                return "There are no Items in that slot";
            }

            Knight knight = FindActiveKnight(userName);
            Item item = items[(int)itemSlot];

            if (knight.HardCoin < item.Cost)
            {
                return "You do not have enough Hard Coins to buy this item. Get a job!";
            }

            Weapon weapon = item as Weapon;
            Spell spell = item as Spell;

            if (weapon != null)
            {
                knight.Weapon = weapon;
            }
            else if (spell != null)
            {
                knight.MagicSpell = spell;
            }

            knight.HardCoin -= item.Cost;
            items.RemoveAt((int)itemSlot);
            TriggerEvent("removeitem " + itemSlot.ToString());

            return knight.Name + " bought " + item.Name + " at a great price! " + knight.Name + " has " + knight.HardCoin + " Hard Coins left!";
        }

        public string PerCommandCheck(string command, string userName, IDictionary<string, string> args)
        {
            if (!string.Equals(command, JoinFellowshipAction, StringComparison.OrdinalIgnoreCase) && FindActiveKnight(userName) == null)
            {
                Knight knight = gameData.FindKnight(userName);

                if (knight != null)
                {
                    activeKnights.Add(knight);
                }
                else
                {
                    return "Sorry, but you have not joined the fellowship yet. Join with " + JoinFellowshipAction;
                }
            }

            return null;
        }

        public void PostCommandUpdate()
        {
            gameData.SaveKnightData();
        }

        public Knight FindActiveKnight(string userName)
        {
            int index = activeKnights.FindIndex(k => k.Name == userName);
            if (index != -1)
            {
                return activeKnights[index];
            }

            return null;
        }

        public Item FindItem(string itemName)
        {
            int index = items.FindIndex(i => i.Name == itemName);
            if (index != -1)
            {
                return items[index];
            }

            return null;
        }

        private void TriggerEvent(string command)
        {
            var args = new OverlayCommandEventArgs();
            args.Command = command;
            VisualUpdate?.Invoke(this, args);
        }

        //Was the monter killed
        public bool CheckMonsterHealth(int monsterIndex)
        {
            Monster monster = monsters[monsterIndex];
            if (monster.Health <= 0)
            {
                monsters.Remove(monster);
                TriggerEvent("monsterkilled " + monsterIndex);
                return true;
            }
            
            TriggerEvent("monsterhit " + monsterIndex);

            return false;
        }
    }

    public class OverlayCommandEventArgs : EventArgs
    {
        public string Command { get; set; }
    }
}
