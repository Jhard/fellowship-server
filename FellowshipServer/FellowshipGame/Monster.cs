﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FellowshipServer.FellowshipGame
{
    public class Monster : LevelableObject
    {
        public string Name { get; private set; }
        public int RewardExp { get; private set; }

        public Monster(string Name, int level) {
            this.Name = Name;
            SetObjectLevel(level);
        }
        public override void SetObjectLevel(int Level)
        {
            base.SetObjectLevel(Level);
            this.Health = (int)(MaxHealth * 0.8);

            double portion = rnd.NextDouble();

            this.Attack = (int)Math.Round(MaxStatIncrease * portion, MidpointRounding.ToNegativeInfinity) + 1;
            this.Defense = (int)Math.Round(MaxStatIncrease * (1 - portion), MidpointRounding.ToNegativeInfinity) + 1;
            this.RewardExp = (int)(MaxExp * 0.75f);
        }

        public string getMonsterBestStat()
        {
            if (Attack > Defense)
            {
                return "Attack";
            }
            else if (Defense > Attack)
            {
                return "Defense";
            }
            else if (Attack == Defense)
            {
                return "Attack/Defense";
            }
            return "Random";
        }
    }

    public class MonsterData
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public Monster CreateMonster()
        {
            return new Monster(Name, Level);
        }
    }
}

/*
WEAPON ATTACK
KNIGHT BASE STAT ATTACK
    MONSTER DEFENSE
    KNIGHT DEFENSE
    WEAPON DEFENSE

DMG TO MONSTER = WEAPON ATTACK * (KNIGHT ATTACK * 0.8) * (KNIGHT ATTACK/(KNIGHT ATTACK + MONSTER DEFENSE))
DMG TO KNIGHT =  (MONSTER ATTACK * 0.8) * (MONSTER ATTACK/(MONSTER ATTACK + KNIGHT DEFENSE))

*/