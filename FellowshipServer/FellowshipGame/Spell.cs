﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FellowshipServer.FellowshipGame
{
    public class Spell : Item
    {
        public int Damage { get; set; }
        public int Heal { get; set; }

        public Spell() : base("nameless", 0)
        { }

        public Spell(string name, int cost, int damage, int heal) : base(name, cost)
        {
            Damage = damage;
            Heal = heal;
        }
    }

    public class SpellData
    {
        public string Name { get; set; }
        public int Damage { get; set; }
        public int Heal { get; set; }
        public int Cost { get; set; }

        public Spell CreateSpell()
        {
            return new Spell(Name, Cost, Damage, Heal);
        }
    }
}
