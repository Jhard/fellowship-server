﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FellowshipServer.FellowshipGame
{
    public class Item
    {
        public string Name { get; protected set; }
        public int Cost { get; protected set; }

        public Item(String Name, int Cost) {
            this.Name = Name;
            this.Cost = Cost;
        }
    }
}
