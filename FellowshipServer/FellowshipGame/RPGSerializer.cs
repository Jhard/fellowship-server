﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace FellowshipServer.FellowshipGame
{
    public class RPGSerializer
    {
        public static readonly string MonsterFile = "Monsters.json";
        public static readonly string KnightFile = "Knight.json";
        public static readonly string WeaponFile = "Weapon.json";
        public static readonly string SpellFile = "Spell.json";

        private List<MonsterData> monsters;
        //private List<KnightData> knights;
        private List<Knight> deserializedKnights;
        private List<WeaponData> weapons;
        private List<SpellData> spells;

        public RPGSerializer()
        {
            monsters = new List<MonsterData>();
            weapons = new List<WeaponData>();
            spells = new List<SpellData>();
            deserializedKnights = new List<Knight>();
        }

        private List<T> LoadResources<T>(string filename)
        {
            if (File.Exists(filename))
            {
                string jsonString = File.ReadAllText(filename);
                return JsonSerializer.Deserialize<List<T>>(jsonString);
            }
            return new List<T>();
        }

        private void SaveResource<T>(string filename, List<T> resourceList)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };
            string jsonString = JsonSerializer.Serialize(resourceList, options);

            File.WriteAllText(filename, jsonString);
        }

        public void LoadAllResources()
        {
            monsters = LoadResources<MonsterData>(MonsterFile);
            weapons = LoadResources<WeaponData>(WeaponFile);
            spells = LoadResources<SpellData>(SpellFile);

            //deserializedKnights
            List<KnightData> knights;
            knights = LoadResources<KnightData>(KnightFile);

            foreach (var knightData in knights)
            {
                deserializedKnights.Add(knightData.CreateKnight());
            }
        }

        public void SaveKnightData()
        {
            List<KnightData> knights = new List<KnightData>();
            foreach (var knight in deserializedKnights)
            {
                knights.Add(new KnightData(knight));
            }
            SaveResource(KnightFile, knights);
        }

        public void AddKnightData(Knight knight)
        {
            deserializedKnights.Add(knight);
        }

        public Knight FindKnight(string name)
        {
            int index = deserializedKnights.FindIndex(k => k.Name == name);
            if (index != -1)
            {
                return deserializedKnights[index];
            }

            return null;
        }

        public Weapon FindWeapon(string name)
        {
            int index = weapons.FindIndex(k => k.Name == name);
            if (index != -1)
            {
                return weapons[index].CreateWeapon();
            }
            
            return null;
        }

        public Spell FindSpell(string name)
        {
            int index = spells.FindIndex(k => k.Name == name);
            if (index != -1)
            {
                return spells[index].CreateSpell();
            }

            return null;
        }

        public Monster FindMonster(string name)
        {
            int index = monsters.FindIndex(k => k.Name == name);
            if (index != -1)
            {
                return monsters[index].CreateMonster();
            }

            return null;
        }
    }
}
