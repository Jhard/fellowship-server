﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FellowshipServer.FellowshipGame
{
    public class LevelableObject
    {
        private int health = 0;

        public int Level { get; protected set; }
        public int CurrentExp { get; protected set; }
        public int MaxExp { get; protected set; }
        public int MaxHealth { get; protected set; }
        public int MaxStatIncrease { get; protected set; }
        public int Attack { get; protected set; }
        public int Defense { get; protected set; }
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                if (value > MaxHealth)
                {
                    health = MaxHealth;
                }
                else if (value < 0)
                {
                    health = 0;
                }
                else
                {
                    health = value;
                }
            }
        }

        public Random rnd = new Random();

        public LevelableObject() { }

        public virtual void SetObjectLevel(int Level) {
            this.Level = Level;
            this.CurrentExp = 0;
            this.MaxExp = (int)(100 * Math.Pow(1.15, Level));
            this.MaxHealth = (int)(10 * Math.Pow(1.30, this.Level - 1));
            this.MaxStatIncrease = (2 * (Level-1));
        }

        public void AddExp(int Exp) {
            if (CurrentExp + Exp > MaxExp)
            {
                SetObjectLevel(this.Level + 1);
            }
            else
            {
                CurrentExp += Exp;
            }
        }
    }
}
