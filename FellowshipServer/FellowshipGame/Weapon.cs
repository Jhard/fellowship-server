﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FellowshipServer.FellowshipGame
{

    public class Weapon : Item
    {
        public int AttackDamage { get; protected set; }
        public int Defense { get; protected set; }

        public Weapon(String Name, int AttackDamage, int Defense, int Cost) : base(Name, Cost)
        {
            this.AttackDamage = AttackDamage;
            this.Defense = Defense;
        }

        public string getStats() {
            return "Weapon Stats - Weapon Attack Damage: " + AttackDamage.ToString() + ", Defense: " + Defense.ToString();
        }

        public string getWeaponBestStat() {
            if (AttackDamage > Defense)
            {
                return "Attack";
            }
            else if (Defense > AttackDamage)
            {
                return "Defense";
            }
            else if (AttackDamage == Defense)
            {
                return "Attack/Defense";
            }
            return "Random";
        }
    }

    public class WeaponData
    {
        public string Name { get; set; }
        public int AttackDamage { get; set; }
        public int Defense { get; set; }
        public int Cost { get; set; }

        public Weapon CreateWeapon()
        {
            return new Weapon(Name, AttackDamage, Defense, Cost);
        }
    }
}
