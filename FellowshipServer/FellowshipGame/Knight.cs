﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FellowshipServer.FellowshipGame
{
    public class Knight : LevelableObject
    {

        public string Name { get; private set; }
        public string Gender { get; private set; }
        public int HardCoin { get; set; }
        public Spell MagicSpell { get; set; }
        public DateTime revivalTimestamp { get; private set; }
        public Weapon Weapon { get; set; }

        public Knight(string name, string gender) : this(name, gender, 1, 1, 1, 10)
        { }

        public Knight(string name, string gender, int level, int attack, int defence, int hardCoin)
        {
            Name = name;
            Gender = gender;
            SetObjectLevel(level);
            Health = MaxHealth;
            Attack = attack;
            Defense = defence;
            HardCoin = hardCoin;
            Weapon = new Weapon("Wooden Sword", 1, 1, 1);
            revivalTimestamp = DateTime.Now.AddMinutes(-2);
        }

        public void AddExperience(int exp, Monster monster)
        {
            if (CurrentExp + exp > MaxExp)
            {
                AddLevelByFightingMonster(monster);
            }
            else {
                CurrentExp += exp;
            }
        }

        public void AddLevelByFightingMonster(Monster monster) {
            SetObjectLevel(Level + 1);
            Health = MaxHealth;
            if (monster.getMonsterBestStat() == "Attack")
            {
                Attack += 2;
            }
            else if (monster.getMonsterBestStat() == "Defense")
            {
                Defense += 2;
            }
            else if (monster.getMonsterBestStat() == "Attack/Defense")
            {
                Attack += 1;
                Defense += 1;
            }
        }

        public void AddLevelByFellowship(int levels) {
            SetObjectLevel(Level + levels);
            Health = MaxHealth;
            if (Weapon.getWeaponBestStat() == "Attack")
            {
                Attack += (2 * levels);
            }
            else if (Weapon.getWeaponBestStat() == "Defense")
            {
                Defense += (2 * levels);
            }
            else if (Weapon.getWeaponBestStat() == "Attack/Defense")
            {
                Attack += levels;
                Defense += levels;
            }
        }

        public string getStats() {
            return "Character Stats - Name: " + Name
                + ", Level: " + Level.ToString()
                + ", Health: " + Health.ToString() + "/" + MaxHealth.ToString()
                + ", Attack: " + Attack.ToString()
                + ", Defense: " + Defense.ToString()
                + ", Exp: " + CurrentExp
                + ", Exp to Level: " + (MaxExp - CurrentExp)
                + ", Hard Coin: " + HardCoin.ToString();
        }
    }

    public struct KnightData
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Level { get; set; }
        public int Exp { get; set; }
        public int HardCoin { get; set; }

        public KnightData(Knight knight)
        {
            Name = knight.Name;
            Gender = knight.Gender;
            Attack = knight.Attack;
            Defense = knight.Defense;
            Level = knight.Level;
            Exp = knight.CurrentExp;
            HardCoin = knight.HardCoin;
        }


        public Knight CreateKnight()
        {
            var knight = new Knight(Name, Gender, Level, Attack, Defense, HardCoin);
            knight.AddExp(Exp);
            return knight;
        }
    }
}
