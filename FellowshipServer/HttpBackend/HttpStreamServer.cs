﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace FellowshipServer.HttpBackend
{
    public class HttpStreamServer
    {
        private StreamReader reader;
        private StreamWriter writer;
        private Dictionary<string, string> urlParameters;
        private Dictionary<string, string> headers;

        public string Method { get; private set; }
        public string Protocol { get; private set; }
        public ReadOnlyDictionary<string, string> UrlParameters { get; private set;}
        public ReadOnlyDictionary<string, string> Headers { get; private set; }

        public HttpStreamServer(Stream stream)
        {
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream);
            urlParameters = new Dictionary<string, string>();
            headers = new Dictionary<string, string>();
            UrlParameters = new ReadOnlyDictionary<string, string>(urlParameters);
            Headers = new ReadOnlyDictionary<string, string>(headers);

            string line = reader.ReadLine();
            System.Console.WriteLine(line);
            while (line != "")
            {
                //look for request line
                if (line.Length > 2 && line.Substring(0, 3).ToUpper() == "GET")
                {
                    string[] split = line.Split(' ');
                    //Method
                    Method = "GET";

                    //URL Parameters
                    for (int i = 1; i < split.Length; i++)
                    {
                        int paramIndex = split[i].IndexOf('?');
                        if (paramIndex != -1)
                        {
                            string[] keyValuePairs = split[i].Substring(paramIndex + 1).Split('&');

                            foreach (string keyValue in keyValuePairs)
                            {
                                string[] keyAndValue = keyValue.Split('=');
                                urlParameters[keyAndValue[0]] = keyAndValue[1];
                            }
                        }

                        //Protocol
                        if (split[i].Contains("HTTP"))
                        {
                            Protocol = split[i];
                        }
                    }
                }
                //Get the header
                else
                {
                    string[] split = line.Split(": ");
                    headers[split[0]] = split[1];
                }

                line = reader.ReadLine();
                System.Console.WriteLine(line);
            }
        }

        public void SendResponce(string data)
        {
            writer.WriteLine("HTTP/1.1 200 OK");
            writer.WriteLine("Content-Type: text/html; charset=utf-8");
            writer.WriteLine("");
            writer.WriteLine(data);
        }

        public void Flush()
        {
            writer.Flush();
        }

        public void Close()
        {
            writer.Close();
        }
    }
}
