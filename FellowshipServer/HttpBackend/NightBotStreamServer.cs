﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FellowshipServer.HttpBackend
{
    public class NightBotStreamServer: HttpStreamServer
    {
        public string UserName { get; private set; }
        public string UserDisplayName { get; private set; }
        public string UserLevel { get; private set; }
        public string ChannelName { get; private set; }
        public string ChannelDisplayName { get; private set; }

        public NightBotStreamServer(Stream stream) : base(stream)
        {
            string userInfoStr = Headers["Nightbot-User"];
            string[] userInfo = userInfoStr.Split('&');

            foreach (string userEle in userInfo)
            {
                string[] keyVal = userEle.Split('=');

                if (keyVal[0] == "name")
                {
                    UserName = keyVal[1];
                }
                else if (keyVal[0] == "displayName")
                {
                    UserDisplayName = keyVal[1];
                }
                else if (keyVal[0] == "userLevel")
                {
                    UserLevel = keyVal[1];
                }
            }

            string channelInfoStr = Headers["Nightbot-Channel"];
            string[] channelInfo = userInfoStr.Split('&');

            foreach (string channelEle in channelInfo)
            {
                string[] keyVal = channelEle.Split('=');

                if (keyVal[0] == "name")
                {
                    ChannelName = keyVal[1];
                }
                else if(keyVal[0] == "displayName")
                {
                    ChannelDisplayName = keyVal[1];
                }
            }

        }
    }
}
