﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FellowshipServer.HttpBackend
{
    public class StreamElementsBotStreamServer : HttpStreamServer
    {
        public string UserName { get; private set; }

        public StreamElementsBotStreamServer(Stream stream) : base(stream)
        {

            UserName = "unknown";
            if (UrlParameters.ContainsKey("user"))
            {
                UserName = UrlParameters["user"];
            }
        }
    }
}
