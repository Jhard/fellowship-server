﻿using FellowshipServer.FellowshipGame;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace FellowshipServer
{
    public class OverlayUpdater
    {
        private TcpClient client;
        private StreamWriter writer;

        public OverlayUpdater(TcpClient _client)
        {
            client = _client;
            writer = new StreamWriter(client.GetStream());
        }

        public void OnOverlayUpdate(object sender, OverlayCommandEventArgs args)
        {
            var rpg = sender as ChatRPG;
            try
            {
                writer.WriteLine(args.Command);
                writer.Flush();
            }
            catch (Exception e)
            {
                rpg.VisualUpdate -= OnOverlayUpdate;
                Console.WriteLine("Overlay Disconnected!\n" + e.ToString());
            }
        }
    }
}
